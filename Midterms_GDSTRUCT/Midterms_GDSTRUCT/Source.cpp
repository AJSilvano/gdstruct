#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	cout << "Enter your desired number of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	system("cls");
	while (true)
	{
		cout << "Unordered Array: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << " ";
		cout << "\nOrdered Array: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << " ";

		cout << endl;

		cout << "\nWhat do you want to do?" << endl;
		cout << "1 - Remove element at index" << endl;
		cout << "2 - Search for element" << endl;
		cout << "3 - Expand and generate random values" << endl;
		cout << "4 - Exit" << endl;

		cout << endl;

		int choice = 0;
		cin >> choice;

		cout << endl;

		if (choice == 1)
		{
			cout << "Enter your number of index you want to remove: ";
			int input;
			cin >> input;
			cout << endl;

			for (int i = 0; i < unordered.getSize(); i++)
			{
				unordered.remove(input);
			}

			for (int i = 0; i < unordered.getSize(); i++)
			{
				ordered.remove(input);
			}

			cout << "Unordered Array: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << " ";
			cout << "\nOrdered Array: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << " ";

			cout << endl;

			system("pause");
			system("cls");
		}

		else if (choice == 2)
		{
			cout << "Enter number to search: ";
			int input;
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			system("pause");
			system("cls");
		}

		else if (choice == 3)
		{
			cout << "Pleae input the size of the expansion: ";
			int inputExpansion;
			cin >> inputExpansion;

			for (int i = 0; i < inputExpansion; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}

			cout << endl;

			cout << "Unordered Array: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << " ";
			cout << "\nOrdered Array: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << " ";

			cout << endl;
			cout << endl;

			system("pause");
			system("cls");
		}

		else
		{
			break;
		}
	}
	system("pause");
}