#include <iostream>
#include <string>
#include "Queue.h"
#include "Stack.h"

using namespace std;

int main()
{
	int desiredChoice = 0;
	int pushedNumber = 0;

	int arraySize = 0;
	int elementSize = 0;

	cout << "Enter you desired number of Array: ";
	cin >> elementSize;

	Queue<int> queue(elementSize);
	Stack<int> stack(elementSize);

	int n = sizeof(queue) / sizeof(queue[0]);


	while (true)
	{
		cout << "What do you want to do?" << endl;
		cout << "1 - Push Elements" << endl;
		cout << "2 - Pop Elements" << endl;
		cout << "3 - Print everything then delete the set" << endl;
		cout << "4 - Exit" << endl;

		cout << endl;

		cout << "Please choose one from the choices: ";
		cin >> desiredChoice;

		cout << endl;

		if (desiredChoice == 1)
		{
			cout << "Please enter your desired number: ";
			cin >> pushedNumber;

			queue.push(pushedNumber);
			//queue.top(pushedNumber);

			stack.push(pushedNumber);
			//stack.top(pushedNumber);

			cout << "Queue: ";
			for (int i = 0; i < queue.getSize(); i++)
				cout << queue[i] << " ";

			cout << "\nStack: ";
			for (int i = stack.getSize() - 1; i >= 0; i--)
				cout << stack[i] << " ";
		}

		else if (desiredChoice == 2)
		{
			cout << "You have popped the front elements." << endl;

			stack.pop();

			queue.pop();

			cout << "Queue: ";
			for (int i = 0; i < queue.getSize(); i++)
				cout << queue[i] << " ";

			cout << "\nStack: ";
			for (int i = stack.getSize() - 1; i >= 0; i--)
				cout << stack[i] << " ";
		}

		else if (desiredChoice == 3)
		{
			cout << "Print the summary" << endl;

			cout << "Queue Elements: " << endl;
			for (int i = 0; i < queue.getSize(); i++)
			{
				cout << queue[i] << endl;
			}

			for (int i = 0; i < queue.getSize(); i++)
			{
				while (true)
				{
					queue.remove(i);
					if (queue.getSize() == NULL)
					{
						break;
					}
				}
			}

			cout << "\nStack Elements: " << endl;
			for (int i = stack.getSize() - 1; i >= 0; i--)
			{
				cout << stack[i] << endl;
			}

			for (int i = 0; i < stack.getSize(); i++)
			{
				while (true)
				{
					stack.remove(i);
					if (stack.getSize() == NULL)
					{
						break;
					}
				}
			}
		}

		else
		{
			break;
		}

		cout << endl;
		
		//Queue Elements:
		//1
		//2
		//3
		//Stack Elements:			Number 3
		//3
		//2
		//1

		//Top element of sets:
		//Queue: 45
		//Stack: 46
		//Press any key to continue . . .
		system("pause");
		system("cls");
	}

	system("pause");
}