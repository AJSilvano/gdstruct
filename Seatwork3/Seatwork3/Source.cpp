#include <iostream>
#include <string>

using namespace std;

void printRecursiveFunction(int number, int sum)
{
	if (number != 0)
	{
		sum += number % 10;
		number /= 10;
		printRecursiveFunction(number, sum);
	}
	else
	{
		cout << "Final Value: " << sum << endl;
	}
}

void fibonacciFunction(int number)
{

	if (number > 0)
	{

		static int first = 0, second = 1, third = 0;

		third = first + second;
		first = second;
		second = third;
		cout << first << " ";
		fibonacciFunction(number - 1);
	}
}

bool primeNumberChecker(int x, int y)
{
	if (x < 2)
		return true;
	if (y == 1)
		return true;
	else
	{
		if (x % y == 0)
			return false;
		else
			return primeNumberChecker(x, y - 1);
	}
}

int main()
{
	// Item #1
	int number = 0;
	int sum = 0;

	cout << "Item #1" << endl;
	cout << "Please enter a value: ";
	cin >> number;

	printRecursiveFunction(number,sum);

	cout << endl;

	//Item #2
	int numberOfElements = 0;

	cout << "Item #2" << endl;
	cout << "Please enter number of elements: ";
	cin >> numberOfElements;

	fibonacciFunction(numberOfElements);

	cout << endl;

	//Item #3
	int oddNumberChecker = 0;

	cout << endl;

	cout << "Item #3" << endl;
	cout << "Please enter the number to be checked: ";
	cin >> oddNumberChecker;

	int primeCheck = primeNumberChecker(oddNumberChecker, oddNumberChecker - 1);

	if (primeCheck == 1)
	{
		cout << "The number you entered is a prime number" << endl;
	}

	else
	{
		cout << "The number you entered is not a prime number" << endl;
	}

	system("pause");
}